
## About App

This is an app that can list, create, update, delete models.

Heroku link; 
https://boiling-thicket-71379.herokuapp.com/

#### Please follow these steps

Please start your local docker daemon.

- ```composer install```
- ```cp .env.example .env```
- edit .env and change cache driver to redis
- https://laravel.com/docs/8.x/sail#configuring-a-bash-alias
- ```sail up -d```
- ```sail artisan key:generate```
- ```sail artisan migrate```
- ```sail artisan db:seed```
- ```sail artisan cache:clear```

To stop simply write ```sail down```
