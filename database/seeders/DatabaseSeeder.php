<?php

namespace Database\Seeders;

use App\Models\Address;
use App\Models\Person;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 10; $i++) {
            Person::factory()
                ->count(1)
                ->has(
                    Address::factory()
                        ->count(rand(2, 5))
                        ->state(function (array $attributes, Person $person) {
                            return ['person_id' => $person->id];
                        })
                )
                ->create();
        }

    }
}
