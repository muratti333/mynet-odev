import ListPeople from "./components/ListPeople";

require('./bootstrap');
import VueRouter from 'vue-router'
import ViewPerson from "./components/ViewPerson";
import ViewAddress from "./components/ViewAddress";

window.Vue = require('vue');
Vue.use(VueRouter);

Vue.component('people-component', require('./components/ListPeople.vue').default);
Vue.component('person-component', require('./components/ViewPerson.vue').default);
Vue.component('address-component', require('./components/ViewAddress.vue').default);
Vue.component('modal', require('./components/Modal.vue').default);

const routes = [
    {
        path: '/',
        component: ListPeople,
        name: 'home'
    },
    {
        path: '/person/:id',
        component: ViewPerson,
        name: 'person' ,
    },
    {
        path: '/address/:address',
        component: ViewAddress,
        name: 'address'
    },
]

const router = new VueRouter({
    routes
})

const app = new Vue({
    router,
    el: '#app',
});
