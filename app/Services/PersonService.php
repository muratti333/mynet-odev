<?php

namespace App\Services;

use App\Constants\CacheConstants;
use Illuminate\Database\Eloquent\Model;

class PersonService extends BaseService
{

    /**
     * @return mixed
     */
    public function getAll()
    {
        if ($this->cacheService->has(CacheConstants::PEOPLE_KEY)) {
            return $this->cacheService->get(CacheConstants::PEOPLE_KEY);
        }
        return $this->cacheService->put(CacheConstants::PEOPLE_KEY, $this->personRepository->getAll(['*'], ['addresses']));

    }

    /**
     * @param $id
     * @return mixed
     */
    public function getById(int $id)
    {
        if ($this->cacheService->has(CacheConstants::PERSON_KEY . '-' . $id)) {
            return $this->cacheService->get(CacheConstants::PERSON_KEY . '-' . $id);
        }
        return $this->cacheService->put(CacheConstants::PERSON_KEY . '-' . $id, $this->personRepository->findById($id, ['*'], ['addresses']));
    }

    /**
     * @param array $request
     * @return Model|mixed
     */
    public function create(array $request)
    {
        return $this->personRepository->create($request);
    }

    /**
     * @param int $id
     * @param array $request
     * @return Model
     */
    public function update(int $id, array $request)
    {
        $person = $this->personRepository->findById($id);
        return $this->personRepository->update($person, $request);
    }

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id)
    {
        $person = $this->personRepository->findById($id);
        return $this->personRepository->delete($person);
    }
}
