<?php

namespace App\Services;

use Illuminate\Support\Facades\Cache;

class CacheService
{
    /**
     * @param $key
     * @param $value
     * @param null $ttl
     * @return bool
     */
    public function put($key, $value, $ttl = null)
    {
        if ($ttl) {
            Cache::put($key, $value, $ttl);
        } else {
            Cache::forever($key, $value);
        }
        return $this->get($key);
    }

    /**
     * @param $key
     * @return mixed
     */
    public function get($key)
    {
        return Cache::get($key);
    }

    /**
     * @param $key
     * @return bool
     */
    public function has($key)
    {
        return Cache::has($key);
    }

    /**
     * @param $key
     * @return bool
     */
    public function forget($key)
    {
        return Cache::forget($key);
    }
}
