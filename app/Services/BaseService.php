<?php

namespace App\Services;

use App\Repositories\Interfaces\AddressRepositoryInterface;
use App\Repositories\Interfaces\PersonRepositoryInterface;

abstract class BaseService
{
    /**
     * @var CacheService
     */
    protected $cacheService;
    /**
     * @var PersonRepositoryInterface
     */
    protected $personRepository;
    /**
     * @var AddressRepositoryInterface
     */
    protected $addressRepository;

    /**
     * BaseService constructor.
     * @param CacheService $cacheService
     * @param PersonRepositoryInterface $personRepository
     * @param AddressRepositoryInterface $addressRepository
     */
    public function __construct(
        CacheService $cacheService,
        PersonRepositoryInterface $personRepository,
        AddressRepositoryInterface $addressRepository
    )
    {
        $this->cacheService = $cacheService;
        $this->personRepository = $personRepository;
        $this->addressRepository = $addressRepository;
    }

    /**
     * @return mixed
     */
    abstract public function getAll();

    /**
     * @param $id
     * @return mixed
     */
    abstract public function getById(int $id);

    /**
     * @param array $request
     * @return mixed
     */
    abstract public function create(array $request);

    /**
     * @param int $id
     * @param array $request
     * @return mixed
     */
    abstract public function update(int $id, array $request);

    /**
     * @param int $id
     * @return mixed
     */
    abstract  public function delete(int $id);
}
