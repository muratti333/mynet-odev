<?php

namespace App\Services;

use App\Constants\CacheConstants;
use Illuminate\Database\Eloquent\Model;

class AddressService extends BaseService
{
    /**
     * @return mixed
     */
    public function getAll()
    {
        if ($this->cacheService->has(CacheConstants::ADDRESS_KEY)) {
            return $this->cacheService->get(CacheConstants::ADDRESS_KEY);
        }
        return $this->cacheService->put(CacheConstants::ADDRESS_KEY, $this->addressRepository->getAll(['*'], ['person']));

    }

    /**
     * @param $id
     * @return mixed
     */
    public function getById(int $id)
    {
        if ($this->cacheService->has(CacheConstants::ADDRESS_KEY . '-' . $id)) {
            return $this->cacheService->get(CacheConstants::ADDRESS_KEY . '-' . $id);
        }
        return $this->cacheService->put(CacheConstants::ADDRESS_KEY . '-' . $id, $this->addressRepository->findById($id, ['*'], ['person']));
    }

    /**
     * @param array $request
     * @return Model|mixed
     */
    public function create(array $request)
    {
        return $this->addressRepository->create($request);
    }

    /**
     * @param int $id
     * @param array $request
     * @return Model
     */
    public function update(int $id, array $request)
    {
        $person = $this->addressRepository->findById($id);
        return $this->addressRepository->update($person, $request);
    }

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id)
    {
        $person = $this->addressRepository->findById($id);
        return $this->addressRepository->delete($person);
    }
}
