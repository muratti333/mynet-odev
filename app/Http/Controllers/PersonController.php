<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePersonRequest;
use App\Services\PersonService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class PersonController extends Controller
{
    /** @var PersonService */
    protected $personService;

    /**
     * PersonController constructor.
     * @param PersonService $personService
     */
    public function __construct(PersonService $personService)
    {
        $this->personService = $personService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        return response()
            ->json($this->personService->getAll());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StorePersonRequest $request
     * @return JsonResponse
     */
    public function store(StorePersonRequest $request)
    {
        return response()
            ->json($this->personService->create($request->all()));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id)
    {
        return response()
            ->json($this->personService->getById($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit(int $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StorePersonRequest $request
     * @param int $id
     * @return Application|ResponseFactory|JsonResponse|Response
     */
    public function update(StorePersonRequest $request, int $id)
    {
        return response()->json($this->personService->update($id, $request->all())->toArray(), 204);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id)
    {
        return response()
            ->json($this->personService->delete($id));
    }
}
