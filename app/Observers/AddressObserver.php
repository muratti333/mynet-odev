<?php

namespace App\Observers;

use App\Constants\CacheConstants;
use App\Models\Address;
use App\Services\CacheService;

class AddressObserver
{
    protected $cacheService;

    public function __construct(CacheService $cacheService)
    {
        $this->cacheService = $cacheService;
    }

    /**
     * Handle the Address "created" event.
     *
     * @param Address $address
     * @param CacheService
     * @return void
     */
    public function created(Address $address)
    {
        $this->cacheService->forget(CacheConstants::ADDRESSES_KEY);
        $this->cacheService->forget(CacheConstants::PERSON_KEY . '-' . $address->person_id);
        $this->cacheService->forget(CacheConstants::PEOPLE_KEY);
    }

    /**
     * Handle the Address "updated" event.
     *
     * @param Address $address
     * @return void
     */
    public function updated(Address $address)
    {
        $this->cacheService->forget(CacheConstants::ADDRESS_KEY . '-' . $address->id);
        $this->cacheService->forget(CacheConstants::ADDRESSES_KEY);
        $this->cacheService->forget(CacheConstants::PERSON_KEY . '-' . $address->person_id);
    }

    /**
     * Handle the Address "deleted" event.
     *
     * @param Address $address
     * @return void
     */
    public function deleted(Address $address)
    {
        $this->cacheService->forget(CacheConstants::ADDRESSES_KEY);
        $this->cacheService->forget(CacheConstants::ADDRESS_KEY . '-' . $address->id);
        $this->cacheService->forget(CacheConstants::PERSON_KEY . '-' . $address->person_id);
        $this->cacheService->forget(CacheConstants::PEOPLE_KEY);
    }
}
