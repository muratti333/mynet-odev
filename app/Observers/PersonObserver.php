<?php

namespace App\Observers;

use App\Constants\CacheConstants;
use App\Models\Person;
use App\Services\CacheService;

class PersonObserver
{
    protected $cacheService;

    public function __construct(CacheService $cacheService)
    {
        $this->cacheService = $cacheService;
    }

    /**
     * Handle the Person "created" event.
     *
     * @param Person $person
     * @param CacheService
     * @return void
     */
    public function created(Person $person)
    {
        $this->cacheService->forget(CacheConstants::PEOPLE_KEY);
    }

    /**
     * Handle the Person "updated" event.
     *
     * @param Person $person
     * @return void
     */
    public function updated(Person $person)
    {
        $this->cacheService->forget(CacheConstants::PERSON_KEY . '-' . $person->id);
        $this->cacheService->forget(CacheConstants::PEOPLE_KEY);
    }

    /**
     * Handle the Person "deleted" event.
     *
     * @param Person $person
     * @return void
     */
    public function deleted(Person $person)
    {
        $this->cacheService->forget(CacheConstants::PERSON_KEY . '-' . $person->id);
        $this->cacheService->forget(CacheConstants::PEOPLE_KEY);
    }
}
