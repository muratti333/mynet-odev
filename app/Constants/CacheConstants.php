<?php

namespace App\Constants;

class CacheConstants
{
    const PEOPLE_KEY = 'people';
    const PERSON_KEY = 'person';

    const ADDRESSES_KEY = 'addresses';
    const ADDRESS_KEY = 'address';
}
