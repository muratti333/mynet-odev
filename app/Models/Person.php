<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Person extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'birthday', 'gender'];

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($person) {
            $person->addresses()->delete();
        });
    }

    /**
     * @return HasMany
     */
    public function addresses()
    {
        return $this->hasMany(Address::class)
            ->orderBy('id', 'DESC');
    }
}
